<?php

namespace App\Http\Controllers;

use App\Models\Administrador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AdministradorController extends Controller
{
    public function index()
    {
        $administradores = Administrador::all();
        return response()->json($administradores);
    }

    public function show($id)
    {
        $administrador = Administrador::find($id);

        if (!$administrador) {
            return response()->json(['message' => 'Administrador no encontrado'], 404);
        }

        return response()->json($administrador);
    }

    public function create(Request $request)
    {
        $rules =  [
            'email' => 'required|email|unique:administradores',
            'password' => 'required|min:6',
        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        $administrador = Administrador::create([
            'email' => $request->input('email'),
            'password' => Hash::make($request->password),
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Usuario registrado correctamente',
            'token' => $administrador->createToken('API TOKEN')->plainTextToken
            ],200);
    }

    public function update(Request $request, $id)
    {
        $administrador = Administrador::find($id);

        if (!$administrador) {
            return response()->json(['message' => 'Administrador no encontrado'], 404);
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:administradores,email,' . $id,
            'password' => 'min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $administrador->update($request->all());

        return response()->json($administrador);
    }

    public function destroy($id)
    {
        $administrador = Administrador::find($id);

        if (!$administrador) {
            return response()->json(['message' => 'Administrador no encontrado'], 404);
        }

        $administrador->delete();

        return response()->json(['message' => 'Administrador eliminado']);
    }
    public function login(Request $request){
        $rules =[
            'email' => 'required|email',
            'password' => 'required',
        ];
        $validator = \Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ],400);
        }
        if(!Auth::attempt($request->only('email','password'))){
            return response()->json([
                'status'=>false,
                'errors'=>['No está autrizado']
            ],401);
        }
        $admin = Administrador::where('email',$request->email)->first();
            return response()->json([
                'status'=>true,
                'message'=>'Usuario logeado',
                'data'=>$admin,
                'token' => $admin->createToken('API TOKEN')->plainTextToken
            ],200);

    }
    public function logout(){
        auth()->user()->tokens()->delete();
        return response()->json([
            'status'=> true,
            'message'=>'Usted ha cerrado sesión'
        ],200);
    } 
}
