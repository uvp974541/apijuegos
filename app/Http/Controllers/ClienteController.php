<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClienteController extends Controller
{
    public function index()
    {
        $clientes = Cliente::all();
        return response()->json($clientes);
    }

    public function show($id)
    {
        $cliente = Cliente::find($id);

        if (!$cliente) {
            return response()->json(['message' => 'Cliente no encontrado'], 404);
        }

        return response()->json($cliente);
    }

    public function store(Request $request)
    {
        $rules= [
            'nombre_cliente' => 'required',
            'email_cliente' => 'required|email|unique:clientes',
            'telefono_cliente' => 'required',
        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        $cliente = Cliente::create($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Cliente registrado correctamente',
            ],200);
    }

    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);

        if (!$cliente) {
            return response()->json(['message' => 'Cliente no encontrado'], 404);
        }

        $validator = Validator::make($request->all(), [
            'nombre_cliente' => 'required',
            'email_cliente' => 'required|email',
            'telefono_cliente' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        $cliente->update($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Datos de cliente actualizados correctamente',
            ],200);
    }

    public function destroy($id)
    {
        try {
            $cliente = Cliente::findOrFail($id);
            $cliente->delete();
            return response()->json(['message' => 'Cliente eliminado']);
        } catch (\Illuminate\Database\QueryException $e) {
            // manejar el error, por ejemplo redirigir con un mensaje de error
            return response()->json([
                'status' => false,
                'errors' => 'No se puede eliminar este cliente'
            ], 404);
        }
    }
}