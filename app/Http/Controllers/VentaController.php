<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VentaController extends Controller
{
    public function index()
    {
        // Obtener todas las ventas con información relacionada
        $ventas = Venta::with(['cliente', 'juego', 'sucursal'])->get();
        return response()->json($ventas);
    }

    public function show($id)
    {
        // Obtener una venta específica con información relacionada
        $venta = Venta::with(['cliente', 'juego', 'sucursal'])->find($id);

        if (!$venta) {
            return response()->json(['errors' => 'Venta no encontrada'], 404);
        }

        return response()->json($venta);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Clientes_idClientes' => 'required|exists:clientes,idClientes',
            'Juegos_idJuegos' => 'required|exists:juegos,idJuegos',
            'Sucursal_idSucursal' => 'required|exists:sucursal,idSucursal',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        // Crear la venta
        $venta = Venta::create($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Venta registrada correctamente',
            ],200);
    }

    public function update(Request $request, $id)
    {
        $venta = Venta::find($id);

        if (!$venta) {
            return response()->json(['errors' => 'Venta no encontrada'], 404);
        }

        $request->validate([
            'Clientes_idClientes' => 'exists:clientes,idClientes',
            'Juegos_idJuegos' => 'exists:juegos,idJuegos',
            'Sucursal_idSucursal' => 'exists:sucursal,idSucursal',
        ]);

        // Actualizar la venta
        $venta->update($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Venta actualizada correctamente',
            ],200);
    }

    public function destroy($id)
    {
        try {
            $venta = Venta::findOrFail($id);
            $venta->delete();
            return response()->json(['message' => 'Venta eliminada']);
        } catch (\Illuminate\Database\QueryException $e) {
            // manejar el error, por ejemplo redirigir con un mensaje de error
            return response()->json([
                'status' => false,
                'errors' => 'No se puede eliminar esta venta'
            ],404);
        }
    }

}
