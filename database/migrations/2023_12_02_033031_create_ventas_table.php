<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id('idVentas');
            $table->unsignedBigInteger('Clientes_idClientes');
            $table->foreign('Clientes_idClientes')->references('idClientes')->on('clientes');            
            $table->unsignedBigInteger('Juegos_idJuegos');
            $table->foreign('Juegos_idJuegos')->references('idJuegos')->on('juegos');
            $table->unsignedBigInteger('Sucursal_idSucursal');
            $table->foreign('Sucursal_idSucursal')->references('idSucursal')->on('sucursal');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
