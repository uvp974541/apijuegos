<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdministradorController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\JuegoController;
use App\Http\Controllers\SucursalController;
use App\Http\Controllers\VentaController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('auth/register',[AdministradorController::class, 'create']);
Route::post('auth/login',[AdministradorController::class, 'login']);
//! Rutas para los Admin
Route::middleware(['auth:sanctum'])->group(function(){
//?Rutas para los Clientes
Route::get('/clientes', [ClienteController::class, 'index']);
Route::get('/clientes/{id}', [ClienteController::class, 'show']);
Route::post('/clientes', [ClienteController::class, 'store']); 
Route::put('/clientes/{id}', [ClienteController::class, 'update']);
Route::delete('/clientes/{id}', [ClienteController::class, 'destroy']);


//*Rutas para los juegos 
Route::get('/juegos', [JuegoController::class, 'index']);
Route::get('/juegos/{id}', [JuegoController::class, 'show']);
Route::post('/juegos', [JuegoController::class, 'store']);
Route::put('/juegos/{id}', [JuegoController::class, 'update']);
Route::delete('/juegos/{id}', [JuegoController::class, 'destroy']);
Route::get('/juegosbyventas', [JuegoController::class, 'juegosByVentas']);

//!Rutas para las sucursales 
Route::get('/sucursales', [SucursalController::class, 'index']);
Route::get('/sucursales/{id}', [SucursalController::class, 'show']);
Route::post('/sucursales', [SucursalController::class, 'store']);
Route::put('/sucursales/{id}', [SucursalController::class, 'update']);
Route::delete('/sucursales/{id}', [SucursalController::class, 'destroy']);

// Ruta para ventas
Route::get('/ventas', [VentaController::class, 'index']);
Route::post('/ventas', [VentaController::class, 'store']); 
Route::get('/ventas/{id}', [VentaController::class, 'show']);
Route::put('/ventas/{id}', [VentaController::class, 'update']);
Route::delete('/ventas/{id}', [VentaController::class, 'destroy']);
Route::get('auth/logout',[AdministradorController::class, 'logout']);

});