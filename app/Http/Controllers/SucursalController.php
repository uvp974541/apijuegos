<?php

namespace App\Http\Controllers;

use App\Models\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SucursalController extends Controller
{
    public function index()
    {
        $sucursales = Sucursal::all();
        return response()->json($sucursales);
    }

    public function show($id)
    {
        $sucursal = Sucursal::find($id);

        if (!$sucursal) {
            return response()->json(['message' => 'Sucursal no encontrada'], 404);
        }

        return response()->json($sucursal);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre_sucursal' => 'required',
            'ubicacion' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        $sucursal = Sucursal::create($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Sucursal registrado correctamente',
            ],200);
    }

    public function update(Request $request, $id)
    {
        $sucursal = Sucursal::find($id);

        if (!$sucursal) {
            return response()->json(['errors' => 'Sucursal no encontrada'], 404);
        }

        $validator = Validator::make($request->all(), [
            'nombre_sucursal' => 'required',
            'ubicacion' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        $sucursal->update($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Sucursal actualizada correctamente',
            ],200);
    }

    public function destroy($id)
    {
        try {
            $sucursal = Sucursal::findOrFail($id);
            $sucursal->delete();
            return response()->json(['message' => 'Sucursal eliminada']);
        } catch (\Illuminate\Database\QueryException $e) {
            // manejar el error, por ejemplo redirigir con un mensaje de error
            return response()->json([
                'status' => false,
                'errors' => 'No se puede eliminar esta sucursal'
            ],404);
        }
    }
}
